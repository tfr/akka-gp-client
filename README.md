# Akka Cluster Client
This is a fork from: https://github.com/r0qs/akka_cluster_client

Provides a cluster oriented client based on Akka Cluster Client module.

Considering that a 'node1' mcgp-replica is running, run with SBT:
sbt 'run-main client.ClientMain 0'
