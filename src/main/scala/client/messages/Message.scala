package client.messages

sealed class ClientMessage extends Serializable
sealed class ConsoleMsg
case class CommandInput(cmdI: String) extends ConsoleMsg
