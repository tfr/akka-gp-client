package client

import akka.actor._
import akka.actor.ExtendedActorSystem
import akka.actor.ActorLogging
import akka.cluster.client.ClusterClient

import scala.concurrent.duration._
import scala.util.Random

import com.typesafe.config.Config
import com.typesafe.config.ConfigObject

import client.messages._

import mcgp.commands._
import mcgp.messages._
import mcgp.serialization.MCGPserializer

trait Client extends ActorLogging {
  this: ClientActor =>

  //TODO: Make it work using the Kryo serializer
  //val serializer = new MCGPserializer(context.system.asInstanceOf[ExtendedActorSystem])
  val console = context.actorOf(Props[ConsoleClient], "console")

  def clientBehavior(proposers: Set[ActorRef]): Receive = {
    
    case StartConsole => console ! StartConsole
    
    case CommandInput(cmdI: String) =>
      log.info("Received COMMAND {} ", cmdI)
      cmdI match {
        //Sends a random command
        case "testing" =>
          if(proposers.nonEmpty) {
            val r_nr = Random.nextInt(100)
            //TODO: The class of the command should be read from the config file
            val r_cmd = new CommutativeCommandInt(r_nr)
            proposers.toVector(Random.nextInt(proposers.size)) ! CommandProposal(r_cmd)
          } else {
            log.warning("Ops!! There are no proposer registered with this client-{}.", id)
          }

        //TODO: Get the number of random commands from the config file
        //Quick hack: I expect that cmdI is the number of command to be sent
        //Sends some random commands
        case _ =>
          if(proposers.nonEmpty) {
            val sq_r_nrs = Seq.fill(cmdI.toInt)(Random.nextInt(100))
            val sq_r_cmds = sq_r_nrs.map(n => new CommutativeCommandInt(n))
            sq_r_cmds.map(c => 
                {
                  log.info("Sending Command {}",c)
                  proposers.toVector(Random.nextInt(proposers.size)) ! CommandProposal(c)
                }
            )
          } else {
            log.warning("Ops!! There are no proposer registered with this client-{}.", id)
          }
      }

    case msg: ClientRegistered =>
      log.info("Client {} registered with {}", id, msg.proposer)
      registerTask.cancel()
      context.become(clientBehavior(proposers + msg.proposer))
    
  }
}

class ClientActor(val id: String, val clusterClient: ActorRef, registerInterval: FiniteDuration) extends Actor with Client {

  import context.dispatcher
  val registerTask = context.system.scheduler.schedule(0.seconds, registerInterval, clusterClient,
    ClusterClient.Send("/user/node", RegisterClient(self), localAffinity = true))

  override def postStop(): Unit = registerTask.cancel()
  def receive = clientBehavior(Set.empty[ActorRef])
}

object ClientActor {
  def props(id: String, clusterClient: ActorRef, registerInterval: FiniteDuration = 10.seconds) : Props =
    Props(new ClientActor(id, clusterClient, registerInterval))
}
